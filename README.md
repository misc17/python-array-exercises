# Python Array Exercises

1. [Remove duplicate numbers that are together (numbers_together.py)](#remove-duplicate-together)
2. [Merge two sorted lists and keep the result sorted (merge_sorted_lists.py)](#merge-sorted-lists)

## Remove duplicate numbers that are together (numbers_together.py)<a name="remove-duplicate-together"></a>

Remove numbers that are duplicate but only if they are next to eachother in an array.

```python
# Ex: 
[1, 2, 2, 3] => [1, 2, 3]
# Ex2: 
[1, 2, 3, 2] => [1, 2, 3, 2]
# Ex3:
[1, 2, 2, 3, 3, 2, 3, 4, 4] => [1, 2, 3, 2, 3, 4]
```

## Merge two sorted lists and keep the result sorted (merge_sorted_lists.py)<a name="merge-sorted-lists"></a>

Merge two sorted lists, but make sure that you keep the end result sorted as well, by comparing the values of both.

This function will run in N time (only 1 loop is performed on both lists)

```python
# Ex:
listA = [1, 3, 5]
listB = [1, 2, 4]

# Result:
listC = [1, 1, 2, 3, 4, 5]
```