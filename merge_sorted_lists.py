listA = [1, 3, 4, 4, 5, 6, 7, 10]
listB = [0, 2, 3, 4, 9, 11]

expectedResult = [0, 1, 2, 3, 3, 4, 4, 4, 5, 6, 7, 9, 10, 11]


# Function to merge 2 sorted lists
def merge_sorted_lists(listA, listB):
    # List that contains the result
    merged_list = []
    # Initialize some indexes to keep track of where we are at in each list
    indexA = indexB = 0

    # Start looping the total number of elements that we have in both lists.
    # Given the Break statements this could be replaced with a While True.
    for i in range(len(listA) + len(listB)):

        # Check if we have reached the end of one list.
        # If that is the case, then simply concatenate the rest of the second list and break out of the loop.
        if indexA >= len(listA):
            merged_list += listB[indexB:]
            break
        elif indexB >= len(listB):
            merged_list += listA[indexA:]
            break

        # Check if the next number in listA is smaller than the one in listB.
        # If it is, then concatenate that one, if not, concatenate the one in listB
        if listA[indexA] < listB[indexB]:
            merged_list.append(listA[indexA])
            indexA += 1
        else:
            merged_list.append(listB[indexB])
            indexB += 1

    # Return the result out of the function
    return merged_list


# Run the function and print the result
print(merge_sorted_lists(listA, listB))
# Also print the expected result, for easier comparison
print(expectedResult)
