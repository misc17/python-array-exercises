# Entire set
my_list = [1, 2, 2, 3, 3, 4, 5, 5, 4]

# Sub Sets with length of 2
# [1, 2] [2, 2] [2, 3] [3, 3] [3, 4] [4, 5] [5, 5] [5, 4]


# Expected Output
expected_output = [1, 2, 3, 4, 5, 4]


# Other entry sets
my_list_2 = [1, 2, 2, 2, 3, 2, 3, 3, 2, 2, 4, 4, 4]
list_2 = [1, 2, 2, 3]


def remove_duplicates_next_to_eachother(item_list):
    # We need to leave this to 2, since we can't skip numbers
    duplicate_range = 2

    # Variable to store the result as we process the entire list
    result = []

    # Temp variable to store the last subset in our list
    _last_set = []

    for i in range(len(item_list) - duplicate_range + 1):
        # Get the subset of the current lup
        subset = item_list[i: i + duplicate_range:]

        # [DEBUG] Print out the transformation
        # print("In: " + str(subset) + " -> " + str(list(dict.fromkeys(subset))))

        if len(result) == 0:            # If we have no data in the result, then add the first number to the result
            result.append(subset[0])
        elif result[-1] != subset[0]:   # Else, if the first number is no the same as the last in the result, add it
            result.append(subset[0])

        # Update the last set
        _last_set = subset

    # Concat the last number in the Temp list only if it is not the same as the last one in the result
    if result[-1] != _last_set[1]:
        result.append(_last_set[1])

    # Return the result
    return result


result = remove_duplicates_next_to_eachother(list_2)
print(result)

result = remove_duplicates_next_to_eachother(my_list)
print(result)

result = remove_duplicates_next_to_eachother(my_list_2)
print(result)

result = remove_duplicates_next_to_eachother(my_list_2)
print(result)